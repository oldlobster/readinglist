//
//  EmptyTable.h
//  ReadList
//
//  Created by david lobo on 21/10/2015.
//  Copyright (c) 2015 David Lobo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmptyTable : UIView

@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
