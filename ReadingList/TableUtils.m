//
//  TableUtils.m
//  ReadList
//
//  Created by david lobo on 21/10/2015.
//  Copyright (c) 2015 David Lobo. All rights reserved.
//

#import "TableUtils.h"
#import "EmptyTable.h"

@implementation TableUtils

+ (void)addBackgroundViewFromNib:(UITableView *)tableView :(NSString *)imageName :(NSString *)message {
    
    UIView *view = [[UIView alloc] init];
    //view.backgroundColor = [UIColor redColor];
    tableView.backgroundView = view;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    EmptyTable *plainView = [[[UINib nibWithNibName:@"EmptyTable" bundle:nil]
                              instantiateWithOwner:nil options:nil] objectAtIndex:0];
    
    if (!imageName) {
        imageName = @"Info-75";
    }
    
    if (!message) {
        message = @"There is no data to display";
    }
    
    plainView.messageLabel.text = message;
    plainView.imageView.image = [UIImage imageNamed:imageName];;
    
    // Some hardcoded layout.
    CGSize padding = (CGSize){ 22.0, 22.0 };
    plainView.frame = (CGRect){padding.width, padding.height, plainView.frame.size};
    
    [tableView.backgroundView addSubview:plainView ];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    // Create the views and metrics dictionaries
    NSDictionary *metrics = @{@"height":@50.0};
    NSDictionary *views = NSDictionaryOfVariableBindings(plainView);
    
    plainView.translatesAutoresizingMaskIntoConstraints = NO;
    
    // Horizontally 30p from the edges
    [tableView.backgroundView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-30.0-[plainView]-30.0-|" options:NO metrics:metrics views:views]];
    
    // Vertiically 15p from label
    [tableView.backgroundView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0.0-[plainView]-0.0-|" options:NO metrics:metrics views:views]];
}

@end
