//
//  BookSearchResultsTableViewController.h
//  Reader
//
//  Created by david lobo on 08/10/2015.
//  Copyright (c) 2015 David Lobo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookSearchResultsTableViewController : UITableViewController

@property (nonatomic, strong) NSMutableArray *searchResults;
@property (nonatomic, strong) BOOL(^searchMoreVolumes)();
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
