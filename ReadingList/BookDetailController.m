//
//  BookDetailController.m
//  ReadingList
//
//  Created by david lobo on 05/10/2015.
//  Copyright (c) 2015 David Lobo. All rights reserved.
//

#import "BookDetailController.h"
#import "BookUtils.h"

@interface BookDetailController ()
@property (weak, nonatomic) IBOutlet UILabel *bookTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *bookAuthorLabel;
@property (weak, nonatomic) IBOutlet UILabel *bookStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *bookDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *bookImageView;
@property (weak, nonatomic) IBOutlet UISwitch *bookStatusSwitch;
@end

@implementation BookDetailController

#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationItem.title = @"Book Info";
    /*UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"Remove" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.rightBarButtonItem = anotherButton;*/
    
    [self.bookStatusSwitch addTarget:self
                 action:@selector(updateBookStatus)
       forControlEvents:UIControlEventValueChanged];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    // NSLog(@"viewDidAppear: %@", self.record);
    UIImage *coverImage = [BookUtils coverImageForItemKey:[self.record valueForKey:@"itemKey"]];
    
    if (coverImage == nil) {
        coverImage = [UIImage imageNamed:@"light_blue_book_cover"];
    }

    self.bookTitleLabel.text = [self.record valueForKey:@"bookTitle"];
    self.bookAuthorLabel.text = [self.record valueForKey:@"bookAuthor"];
    self.bookDescriptionLabel.text = [self.record valueForKey:@"bookDescription"];
    self.bookImageView.image = coverImage;
    
    NSInteger bookStatus = [[self.record valueForKey:@"bookStatus"] integerValue];
    
    if (bookStatus == 1) {
        [self.bookStatusSwitch setOn:YES animated:NO];
    } else {
        [self.bookStatusSwitch setOn:NO animated:NO];
    }
}

-(void) viewWillDisappear:(BOOL)animated {
    if ([self.navigationController.viewControllers indexOfObject:self] == NSNotFound) {
        [self reloadReadingListTable];
    }
    [super viewWillDisappear:animated];
}

#pragma mark - Custom Methods

- (void)updateBookStatus {
    NSString *bookStatus;
    
    // 1 means complete
    // 0 means incomplete
    if (self.bookStatusSwitch.isOn) {
        bookStatus = @"YES";
        //self.book.bookStatus = [NSNumber numberWithInt:1];
        [self.record setValue:[NSNumber numberWithInt:1] forKey:@"bookStatus"];
    
    } else {
        bookStatus = @"NO";
        //self.book.bookStatus = [NSNumber numberWithInt:0];
        [self.record setValue:[NSNumber numberWithInt:0] forKey:@"bookStatus"];
    }
    
    // Save Record
    NSError *error = nil;
    
    if ([self.managedObjectContext save:&error]) {
        // Pop View Controller
        NSLog(@"Book Status Changed to %@", bookStatus);
    } else {
        if (error) {
            NSLog(@"Unable to save record.");
            NSLog(@"%@, %@", error, error.localizedDescription);
        }
        
        // Show Alert View
        [[[UIAlertView alloc] initWithTitle:@"Warning" message:@"Your to-do could not be saved." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
}

#pragma mark - Navigation

-(IBAction)prepareForUnwind:(UIStoryboardSegue *)segue {
    /*if ([segue.identifier isEqualToString:@"unwindToViewController1"]) {
        ThreeViewController *threeVC = (ThreeViewController *)segue.sourceViewController;
        NSLog(@"Violets are %@", threeVC.violetsAreColor);
    }*/
}

@end
