//
//  BookUtils.h
//  ReadList
//
//  Created by david lobo on 19/10/2015.
//  Copyright (c) 2015 David Lobo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface BookUtils : NSObject

+ (BOOL)downloadCoverImage:(NSString *)urlString :(NSString *)itemKey;
+ (NSString *)imagePathForKey:(NSString *)itemKey;
+ (UIImage *)coverImageForItemKey:(NSString *)itemKey;
+ (void)deleteImageForItemKey: (NSString *)itemKey;

@end


