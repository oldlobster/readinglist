//
//  BookSearchTableViewController.m
//  Reader
//
//  Created by david lobo on 08/10/2015.
//  Copyright (c) 2015 David Lobo. All rights reserved.
//

#import "BookSearchTableViewController.h"
#import "BookSearchResultsTableViewController.h"
#import "ReadingListAddCell.h"
#import "GTLBooks.h"
#import "TableUtils.h"

@interface BookSearchTableViewController () <UISearchResultsUpdating>

@property (nonatomic, strong) NSArray *bookData;
@property (nonatomic, strong) NSArray *books;
@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, strong) NSMutableArray *searchResults;
@property (nonatomic) BOOL useTestData;

// GTL Service
@property (nonatomic) GTLServiceBooks *booksService;
@property (nonatomic, retain) GTLBooksVolumes *publicVolumes;
@property (nonatomic, retain) GTLServiceTicket *publicVolumesTicket;
@property (nonatomic, retain) NSError *publicVolumesFetchError;
@property (nonatomic, retain) GTLBooksBookshelves *bookshelves;
@property (nonatomic, retain) GTLServiceTicket *bookshelvesTicket;
@property (nonatomic, retain) GTLBooksVolumes *myVolumes;
@property (nonatomic, retain) GTLServiceTicket *myVolumesTicket;
@property (nonatomic, retain) NSError *myVolumesFetchError;
@property (nonatomic, retain) GTLServiceTicket *editMyVolumeTicket;

@property (nonatomic) NSUInteger loadMoreItemsRetryMax;
@property (nonatomic) NSUInteger itemsCount;
@property (nonatomic) NSUInteger totalItemsCount;
@property (nonatomic) NSUInteger maxItems;
@property (nonatomic) NSUInteger startIndex;
@property (nonatomic) NSString *searchTerm;

// total items in the table
@property (nonatomic) NSUInteger totalItemsLoaded;
@property (nonatomic) BOOL loadMoreInProgress;
@property (nonatomic) BOOL needAutoloadMore;
@property (nonatomic) NSUInteger loadMoreItemsRetryCount;
@property (nonatomic) BOOL networkActivity;

@property (nonatomic) NSUInteger totalGoogleAPICallsSinceUserAction;

@end

@implementation BookSearchTableViewController

#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.useTestData = NO;
    self.needAutoloadMore = NO;
    self.loadMoreInProgress = NO;
    self.loadMoreItemsRetryMax = 3;
    self.totalItemsLoaded = 0;
    self.maxItems = 10;
    self.loadMoreItemsRetryCount = 0;
    self.networkActivity = NO;
    self.totalGoogleAPICallsSinceUserAction = 0;
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.books = @[];
    
    if (self.useTestData) {
        // Get local json file we'll be using to populate our TableView
        NSString *path = [[NSBundle mainBundle] pathForResource:@"bookData" ofType:@"json"];
        NSData *data = [NSData dataWithContentsOfFile:path];
        NSError *error;
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        //self.airlines = dict[@"airlines"];
        self.bookData = dict[@"books"];
    }
    
    // NEW CODE
    // There's no transition in our storyboard to our search results tableview or navigation controller
    // so we'll have to grab it using the instantiateViewControllerWithIdentifier: method
    UINavigationController *searchResultsController = [[self storyboard] instantiateViewControllerWithIdentifier:@"TableSearchResultsNavigationController"];
    
    // Our instance of UISearchController will use searchResults
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:searchResultsController];
    
    // The searchcontroller's searchResultsUpdater property will contain our tableView.
    self.searchController.searchResultsUpdater = self;
    
    // The searchBar contained in XCode's storyboard is a leftover from UISearchDisplayController.
    // Don't use this. Instead, we'll create the searchBar programatically.
    self.searchController.searchBar.frame = CGRectMake(self.searchController.searchBar.frame.origin.x,
                                                       self.searchController.searchBar.frame.origin.y,
                                                       self.searchController.searchBar.frame.size.width, 44.0);
    
    self.searchController.searchBar.searchBarStyle = UISearchBarStyleMinimal;
    //self.searchController.searchBar.barTintColor = [UIColor colorWithRed:(236.0/255.0) green:(239.0/255.0) blue:(241.0/255.0) alpha:1.0];
    self.searchController.searchBar.backgroundColor = [UIColor colorWithRed:(236.0/255.0) green:(239.0/255.0) blue:(241.0/255.0) alpha:1.0];
    
    self.searchController.searchBar.delegate = self;
   
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    
    //init of GTL Services
    self.booksService = [[GTLServiceBooks alloc] init];
    
    // Have the service object set tickets to fetch consecutive pages
    // of the object so we do not need to manually fetch them
    self.booksService.shouldFetchNextPages = YES;
    
    // Have the service object set tickets to retry temporary error conditions
    // automatically
    self.booksService.retryEnabled = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 67;
}

#pragma mark - UITableViewDatasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([self.books count] == 0) {
        if (self.tableView.backgroundView == nil) {
            self.tableView.backgroundView = [[UIView alloc] init];
        }
        
        NSString *imageName = @"Search-75";
        NSString *message = @"Search books to add them to your reading list";
        [TableUtils addBackgroundViewFromNib:self.tableView :imageName :message];
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.books count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Reimplement to show a initial list when search table first appears
    
    /*NSDictionary *book = [self.books objectAtIndex:indexPath.row];
    
    if (book == nil || [book count] == 0) {
        return;
    }
    
    ReadingListCell *myCell = (ReadingListCell *)cell;

    myCell.bookAuthorLabel.text = [book objectForKey:@"author"];
    myCell.bookTitleLabel.text = [book objectForKey:@"title"];

    NSString *coverImageName = [book objectForKey:@"image"];
    UIImage *coverImage = [UIImage imageNamed:coverImageName];
    
    if (coverImage != nil) {
        myCell.bookImageView.image = coverImage;
    }*/
}

#pragma mark - UISearchbarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    //NSLog(@"Text Change");
}

#pragma mark - UISearchControllerDelegate & UISearchResultsDelegate

// Called when the search bar becomes first responder
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    // Add implementation to make the search box an autocomplete
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    self.totalGoogleAPICallsSinceUserAction = 0;
    [self updateFilteredContentForSearchTerm:searchBar.text];
}

#pragma mark - Custom

- (void)updateFilteredContentForSearchTerm:(NSString *)searchTerm {
    
    self.needAutoloadMore = NO;
    self.searchTerm = searchTerm;
    self.startIndex = 0;
    
    dispatch_queue_t searchQueue = dispatch_queue_create("com.readinglist.search", 0);
    
    dispatch_async(searchQueue, ^{
        if (self.useTestData) {
            [self searchBooksWithRemoteAPI:searchTerm :0 :10];
        } else {
            [self searchBooksWithGoogleAPI:searchTerm :0 :10];
        }
    });
}

- (void)updateTableWithResult:(NSDictionary *)resultsObject :(BOOL)appendRows {
    if (self.searchController.searchResultsController) {
        //NSLog(@"Reload");
        UINavigationController *navController = (UINavigationController *)self.searchController.searchResultsController;
        
        // Present SearchResultsTableViewController as the topViewController
        BookSearchResultsTableViewController *vc = (BookSearchResultsTableViewController *)navController.topViewController;
        
        NSArray *items = [resultsObject valueForKey:@"items"];
        // Update searchResults
        if (!appendRows) {
            NSLog(@"Creating search results array");
            vc.searchResults = [items mutableCopy] ;
            self.totalItemsLoaded = [items count];
        } else {
            NSLog(@"Adding to search results array");
            for (NSDictionary *book in items) {
                [vc.searchResults addObject:book];
                self.totalItemsLoaded++;
            }
        }
        
        NSNumber *itemsCountNumber = (NSNumber *)[resultsObject objectForKey:@"items_count"];
        NSNumber *totalItemsCountNumber = (NSNumber *)[resultsObject objectForKey:@"total_items_count"];
        
        self.itemsCount = 0;
        self.totalItemsCount = 0;
        
        if (itemsCountNumber != nil) {
            self.itemsCount = [itemsCountNumber integerValue];
        }
        
        if (totalItemsCountNumber != nil) {
            self.totalItemsCount = [totalItemsCountNumber integerValue];
        }
        
        self.needAutoloadMore = NO;
        
        // Reset if a new search was performed
        if (self.startIndex == 0) {
            self.loadMoreItemsRetryCount = 0;
        }
        
        if (self.totalItemsLoaded != 0 && self.totalItemsCount != 0) {
            if (self.totalItemsCount > self.totalItemsLoaded) {
                NSLog(@"setting needAutoloadmore to YES");
                self.needAutoloadMore = YES;
                self.startIndex = self.startIndex + self.maxItems;
            }
        }
        
        self.itemsCount = [(NSNumber *)[resultsObject valueForKey:@"items_count"] integerValue];
        self.totalItemsCount = [(NSNumber *)[resultsObject valueForKey:@"total_items_count"] integerValue];
        
        NSLog(@"total_items_loaded=%lu", (unsigned long)self.totalItemsLoaded);
        NSLog(@"items_count=%lu", (unsigned long)self.itemsCount);
        NSLog(@"total_items_count=%lu", (unsigned long)self.totalItemsCount);
        NSLog(@"start_index=%lu", (unsigned long)self.startIndex);
        
        if (appendRows) {
            NSLog(@"Append Rows is set");
        } else {
            NSLog(@"Append Rows not set");
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [vc.tableView reloadData];
        });
    }
}

- (void)loadMoreBooks {
    self.loadMoreItemsRetryCount++;
    NSLog(@"Load more books called: %lu", (unsigned long)self.loadMoreItemsRetryCount);
    
    if (self.needAutoloadMore) {
        NSLog(@"Load more books is needed");
        if (!self.networkActivity) {
            NSLog(@"No network activity");
            
            if (self.loadMoreItemsRetryCount >= 5) {
                NSLog(@"Load more books called too many (%lu) times - max is 5", (long)self.loadMoreItemsRetryCount);
            } else {
                dispatch_queue_t loadmoreQueue = dispatch_queue_create("com.readinglist.loadmore", 0);
                dispatch_async(loadmoreQueue, ^{
                    if (self.useTestData) {
                        [self searchBooksWithRemoteAPI: self.searchTerm :self.startIndex :self.maxItems];
                    } else {
                        [self searchBooksWithGoogleAPI:self.searchTerm :self.startIndex :self.maxItems];
                    }
                });
            }
        } else {
            NSLog(@"Network activity in progress");
        }
    } else {
        NSLog(@"Load more books is not needed");
    }
}

- (void)searchBooksWithGoogleAPI:(NSString *)text :(NSUInteger)startIndex :(NSInteger)maxItems
{
    self.totalGoogleAPICallsSinceUserAction++;
    
    if (self.totalGoogleAPICallsSinceUserAction >= 5) {
        NSLog(@"Search Google API called too many times (%lu) without user action",  (long)self.totalGoogleAPICallsSinceUserAction);
    } else {
        NSLog(@"Searching for %@ startIndex: %lu", text, (long)startIndex);
        self.publicVolumes = nil;
        self.publicVolumesFetchError = nil;
        
        //NSString *exactMatch = [NSString stringWithFormat:@"\"%@\"", text];
        
        GTLQueryBooks *query = [GTLQueryBooks queryForVolumesListWithQ:text];
        query.maxResults = maxItems;
        query.startIndex = startIndex;
        query.projection = kGTLBooksProjectionLite;
        query.fields = @"items(volumeInfo(authors,description,imageLinks,subtitle,title)),totalItems";
        //query.filter = kGTLBooksFilterFreeEbooks;
        
        // The Books API currently requires that search queries not have an
        // authorization header (b/4445456)
        query.shouldSkipAuthorization = YES;
        
        // Note: setting the APIKey on the service object may give a higher
        // server quota when executing unauthorized queries
        GTLServiceBooks *service = self.booksService;
        [service setAPIKey:@"AIzaSyDHS3M9Q3E5jhM80yOkKmk0yT3PJ-p9K34"];
        
        self.networkActivity = YES;
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        
        self.publicVolumesTicket = [service executeQuery:query
                                       completionHandler:^(GTLServiceTicket *ticket,
                                                           id object, NSError *error) {
                                           
                                           self.networkActivity = NO;
                                           [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                                           
                                           // callback
                                           self.publicVolumes = object;
                                           self.publicVolumesFetchError = error;
                                           self.publicVolumesTicket = nil;
                                           
                                           NSLog(@"%@", error);
                                           
                                           //NSLog(@"%@", object);
                                           GTLBooksVolumes *results = object;
                                           
                                           NSMutableArray *items = [[NSMutableArray alloc] init];
                                           
                                           for (GTLBooksVolume *volume in results) {
                                               //NSLog(@"%@", volume);
                                               //NSLog(@"%@", volume.volumeInfo.title);
                                               //NSLog(@"%@", volume.volumeInfo.imageLinks);
                                               
                                               GTLBooksVolumeVolumeInfo *vi = volume.volumeInfo;
                                               
                                               NSString *title;
                                               NSString *author = @"";
                                               NSString *description = @"";
                                               NSString *imageLink = @"";
                                               
                                               title = volume.volumeInfo.title;
                                               
                                               if (vi.descriptionProperty != nil && [vi.descriptionProperty length] > 0) {
                                                   description = vi.descriptionProperty;
                                               }
                                               
                                               if (vi.authors != nil) {
                                                   NSArray *authors = (NSArray *)vi.authors;
                                                   if ([authors count] > 0) {
                                                       if (authors[0] != nil && [(NSString *)authors[0] length] > 0) {
                                                           author = authors[0];
                                                       }
                                                   }
                                               }
                                               
                                               if (volume.volumeInfo.imageLinks.thumbnail != nil && [volume.volumeInfo.imageLinks.thumbnail length] > 0) {
                                                   imageLink = volume.volumeInfo.imageLinks.thumbnail;
                                               }
                                               
                                               if (title != nil && [title length] > 0) {
                                                   NSDictionary *newBook = @{@"title": title,
                                                                             @"author": author,
                                                                             @"image_url": imageLink,
                                                                             @"description": description};
                                                   
                                                   [items addObject:newBook];
                                               }
                                           }
                                           
                                           if ([results totalItems] && [results totalItems] > 0 && results.items && [results.items count] >0) {
                                               NSUInteger itemsCount = [results.items count];
                                               NSUInteger totalItemsCount = [[results totalItems] intValue];
                                               
                                               NSLog(@"Items: %lu", (unsigned long)itemsCount);
                                               NSLog(@"Total Items%lu", (unsigned long)totalItemsCount);
                                               
                                               NSDictionary *result = @{@"items_count": [NSNumber numberWithInteger:itemsCount],
                                                                        @"total_items_count": [NSNumber numberWithInteger:totalItemsCount],
                                                                        @"items": [items copy]};
                                               
                                               BOOL appendRows = startIndex == 0 ? NO : YES;
                                               [self updateTableWithResult:result :appendRows];
                                           }
                                       }];
    }
}

- (void)searchBooksWithRemoteAPI:(NSString *)text :(NSUInteger)startIndex :(NSInteger)maxItems
{
    NSString *baseUrl = @"http://local.test.com/readinglist/search.php";
    NSString *urlString = [NSString stringWithFormat:@"%@?search_term=%@&start_index=%lu", baseUrl, text, (unsigned long)startIndex];
    
    NSLog(@"%@", urlString);
    
    self.networkActivity = YES;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSURL *url = [NSURL URLWithString:urlString];
    //NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval: 30];
    
    // Send a synchronous request
    NSURLRequest * request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30];
    NSURLResponse * response = nil;
    NSError * error = nil;
    NSData * data = [NSURLConnection sendSynchronousRequest:request
                                          returningResponse:&response
                                                      error:&error];
    
    // Parse data here
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    self.networkActivity = NO;
    
    if (error == nil)
    {
        if (startIndex > 0) {
            self.loadMoreInProgress = NO;
        }
        if (data.length > 0 && error == nil)
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                   options:0
                                                                     error:NULL];
            //NSLog(@"%@", result);
            if (result) {
                if ([result objectForKey:@"items_count"]
                    && [result objectForKey:@"total_items_count"]
                    && [result objectForKey:@"items"]) {
                    
                    BOOL appendRows = startIndex == 0 ? NO : YES;
                    
                    [self updateTableWithResult:result :appendRows];
                    
                }
            }
        }
    } else {
        NSLog(@"There was a problem connecting to network");
    }
}

- (IBAction)cancel:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

@end