//
//  BookUtils.m
//  ReadList
//
//  Created by david lobo on 19/10/2015.
//  Copyright (c) 2015 David Lobo. All rights reserved.
//

#import "BookUtils.h"
#import <UIKit/UIKit.h>

@implementation BookUtils

+ (BOOL)downloadCoverImage:(NSString *)urlString :(NSString *)itemKey {
    // Image Download
    
    if (urlString && [urlString length] > 0) {
        
        NSURL *url = [NSURL URLWithString: urlString];
        /* @"https://books.google.co.uk/books/content?id=9fBuFuklv-sC&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE71gRV3NEDyVJ-mLv4somlE7n3hqfscsJV3VVh_5s4PfoBDjjkydMi8dwo8Hmxur5BqkJhatp5Z7PHoYklZCGsLZn5TioS9idhgrToGNKjuhAQnxmVH61iY4Oomfdgi7701BbOwJ"];*/
        UIImage *image = [UIImage imageWithData: [NSData dataWithContentsOfURL:url]];
        
        // Create path.
        NSString *filePath = [self imagePathForKey:itemKey];
        
        // Save image.
        [UIImagePNGRepresentation(image) writeToFile:filePath atomically:YES];
        
        NSLog(@"%@",[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]);
        return YES;
    }
    
    // EOF Image Download
    
    return NO;
}

+ (NSString *) imagePathForKey:(NSString *)itemKey {
    // Create path.
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:[itemKey stringByAppendingString:@".png"]];
    return filePath;
}

+ (UIImage *)coverImageForItemKey:(NSString *)itemKey
{
    //NSLog(@"bookCoverImage");
    if (itemKey) {
        
        // Getting the image from documents dir
        NSString *pngFile = [BookUtils imagePathForKey:itemKey];
        
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:pngFile];
        
        if (fileExists) {
            //NSLog(@"fileexists");
            UIImage *bookImage = [UIImage imageNamed:pngFile];
            return bookImage;
        } else {
            //NSLog(@"No fileexists");
        }
    }
    
    UIImage *bookImage = [UIImage imageNamed:@"light_blue_book_cover"];
    return bookImage;
}

+ (void)deleteImageForItemKey: (NSString *)itemKey {
    // Getting the image from documents dir
    NSString *pngFile = [BookUtils imagePathForKey:itemKey];
    
    // Save image.
    //[UIImagePNGRepresentation(image) writeToFile:filePath atomically:YES];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    //NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:pngFile error:&error];
    if (success) {
        NSLog(@"File Deleted: %@", pngFile);
        /*UIAlertView *removeSuccessFulAlert=[[UIAlertView alloc]initWithTitle:@"Congratulation:" message:@"Successfully removed" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
         [removeSuccessFulAlert show];*/
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}

@end
