//
//  GlobalVars.h
//  FootyApp
//
//  Created by david lobo on 05/08/2015.
//  Copyright (c) 2015 Big Nerd Ranch. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GlobalVars : NSObject
{
    NSString *_env;

}

+ (GlobalVars *)sharedInstance;

@property(strong, nonatomic, readwrite) NSString *env;

@end