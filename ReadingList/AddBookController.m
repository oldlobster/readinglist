//
//  AddBookController.m
//  ReadingList
//
//  Created by david lobo on 06/10/2015.
//  Copyright (c) 2015 David Lobo. All rights reserved.
//

#import "AddBookController.h"
#import <CoreData/CoreData.h>
#import "BookUtils.h"

@interface AddBookController ()

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *bookTitleInput;
@property (weak, nonatomic) IBOutlet UITextField *bookAuthorInput;
@property (weak, nonatomic) IBOutlet UITextView *bookDescriptionInput;
@property (weak, nonatomic) IBOutlet UITextField *imageUrlInput;
@property (nonatomic) UIView *activeTextField;
@property (nonatomic) CGSize keyboardSize;
@property (nonatomic) NSString *validationMessage;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end

@implementation AddBookController

#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.bookTitleInput.delegate = self;
    self.bookAuthorInput.delegate = self;
    self.bookDescriptionInput.delegate = self;
    self.imageUrlInput.delegate = self;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [self.bookTitleInput becomeFirstResponder];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if ([self.bookTitleInput isFirstResponder]) {
        
        [self.bookAuthorInput becomeFirstResponder];
    } else if ([self.bookAuthorInput isFirstResponder]) {
        
        [self.imageUrlInput becomeFirstResponder];
    } else if ([self.imageUrlInput isFirstResponder]) {
        [self.bookDescriptionInput becomeFirstResponder];
    } else {
    
    [textField resignFirstResponder];
    }
    
    [self scrollToActiveField];
    
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    self.activeTextField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    self.activeTextField = nil;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    self.activeTextField = textView;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"Done"]) {
        NSLog(@"Done - send params to dest view controller");
        //NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        //RecipeDetailViewController *destViewController = segue.destinationViewController;
       // destViewController.recipeName = [recipes objectAtIndex:indexPath.row];
    }
}

#pragma mark - Custom methods

- (BOOL) validateFields
{
    if (self.bookTitleInput.text.length > 0 && self.bookAuthorInput.text.length > 0) {
        return YES;
    } else {
        
        if (self.bookTitleInput.text.length <= 0 && self.bookTitleInput.text.length <= 0) {
            self.validationMessage = @"Please enter title and autor";
        } else if (self.bookTitleInput.text.length <= 0) {
            self.validationMessage = @"Please enter title";
        } else {
            self.validationMessage = @"Please enter author";
        }
        
        return NO;
    }
}

/*- (BOOL) saveBookToStore
{
    Book *newBook = [[BookStore sharedStore] createBook];
    
    newBook.bookTitle = self.bookTitleInput.text;
    newBook.bookAuthor = self.bookAuthorInput.text;
    newBook.bookDescription = self.bookDescriptionInput.text;
    
    NSLog(@"Saving book data...");
    NSLog(@"title:%@", self.bookTitleInput.text);
    NSLog(@"author:%@", self.bookAuthorInput.text);
    NSLog(@"desciption:%@", self.bookDescriptionInput.text);
    
    BOOL success = [[BookStore sharedStore] saveChanges];
    
    NSLog(@"UUID:%@", newBook.itemKey);
    
    // Image Download
    
    if ([self.imageUrlInput.text length] > 0) {
        
        NSURL *url = [NSURL URLWithString: self.imageUrlInput.text];
        // @"https://books.google.co.uk/books/content?id=9fBuFuklv-sC&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE71gRV3NEDyVJ-mLv4somlE7n3hqfscsJV3VVh_5s4PfoBDjjkydMi8dwo8Hmxur5BqkJhatp5Z7PHoYklZCGsLZn5TioS9idhgrToGNKjuhAQnxmVH61iY4Oomfdgi7701BbOwJ"];
        UIImage *image = [UIImage imageWithData: [NSData dataWithContentsOfURL:url]];
        
        // Create path.
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:[newBook.itemKey stringByAppendingString:@".png"]];
        
        // Save image.
        [UIImagePNGRepresentation(image) writeToFile:filePath atomically:YES];
        
        NSLog(@"%@",[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]);
    }
    
    // EOF Image Download
    
    return success;
}*/

-(IBAction)done:(id)sender
{
    NSLog(@"Save Book");
    
    if (![self validateFields]) {
        
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missing Book Info"
                                                        message:self.validationMessage
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];

        
    } else {
        //[self saveBookToStore];
        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    }
}


- (IBAction)save:(id)sender {
    // Helpers
    NSString *title = self.bookTitleInput.text;
    NSString *description = self.bookDescriptionInput.text;
    
    NSUUID *uuid = [[NSUUID alloc] init];
    NSString *key  = [uuid UUIDString];
    
    if (title && title.length) {
        // Create Entity
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Book" inManagedObjectContext:self.managedObjectContext];
        
        // Initialize Record
        NSManagedObject *record = [[NSManagedObject alloc] initWithEntity:entity insertIntoManagedObjectContext:self.managedObjectContext];
        
        // Populate Record
        [record setValue:title forKey:@"bookTitle"];
        [record setValue:[NSDate date] forKey:@"createdAt"];
        [record setValue:key forKey:@"itemKey"];
        [record setValue:description forKey:@"bookDescription"];
        
        // Save Record
        NSError *error = nil;
        
        
        // Image Download
        
        [BookUtils downloadCoverImage:self.imageUrlInput.text :key];
    
        
        if ([self.managedObjectContext save:&error]) {
            NSLog(@"Saved Record");
            
            
            // Dismiss View Controller
            [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
            
        } else {
            
            //TODO - Delete the image if the record saving fails
            
            if (error) {
                NSLog(@"Unable to save record.");
                NSLog(@"%@, %@", error, error.localizedDescription);
            }
            
            // Show Alert View
            [[[UIAlertView alloc] initWithTitle:@"Warning" message:@"Your to-do could not be saved." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
        
    } else {
        // Show Alert View
        [[[UIAlertView alloc] initWithTitle:@"Warning" message:@"Your to-do needs a name." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
}

-(IBAction)cancel:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Keyboard

- (void)keyboardWasShown:(NSNotification *)notification {
    NSLog(@"keyboard %f", self.activeTextField.frame.origin.y);
    // Step 1: Get the size of the keyboard.
    self.keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;

    [self scrollToActiveField];
}

- (void) keyboardWillHide:(NSNotification *)notification {
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

#pragma mark - Custom

- (void) scrollToActiveField {
    
    NSLog(@"%@", self.activeTextField);
    // Step 2: Adjust the bottom content inset of your scroll view by the keyboard height.
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, self.keyboardSize.height + 16, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    
    // Step 3: Scroll the target text field into view.
    CGRect aRect = self.view.frame;
    
    NSLog(@"TextView Y=%f, Height=%f", self.activeTextField.frame.origin.y, self.activeTextField.frame.size.height);
    aRect.size.height -= self.keyboardSize.height + self.activeTextField.frame.size.height;
    if (!CGRectContainsPoint(aRect, self.activeTextField.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, (self.activeTextField.frame.origin.y + self.activeTextField.frame.size.height) - (self.keyboardSize.height-15));
        [self.scrollView setContentOffset:scrollPoint animated:YES];
    }
}

-(void)dismissKeyboard {
    [self.activeTextField resignFirstResponder];
}

@end
