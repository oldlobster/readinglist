//
//  BookSearchResultsTableViewController.m
//  Reader
//
//  Created by david lobo on 08/10/2015.
//  Copyright (c) 2015 David Lobo. All rights reserved.
//

#import "BookSearchResultsTableViewController.h"
#import "ReadingListAddCell.h"
#import <CoreData/CoreData.h>
#import "BookUtils.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ReadingListController.h"
#import "BookSearchTableViewController.h"

@interface BookSearchResultsTableViewController ()

@property (nonatomic, strong) NSArray *array;

@end

@implementation BookSearchResultsTableViewController

#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UINavigationController *rootNav = (UINavigationController *)self.presentingViewController.presentingViewController;
    ReadingListController *rl = (ReadingListController *)[rootNav.viewControllers objectAtIndex:0];
    
    self.managedObjectContext = rl.managedObjectContext;

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.searchResults count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *nibFile = @"ReadingListAddCell";
    ReadingListAddCell *cell = [tableView dequeueReusableCellWithIdentifier:nibFile];
    
    if (!cell) {
        [tableView registerNib:[UINib nibWithNibName:nibFile bundle:nil] forCellReuseIdentifier:nibFile];
        cell = [tableView dequeueReusableCellWithIdentifier:nibFile];
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == [self.searchResults count] -1) {
        UINavigationController *nav = (UINavigationController *)self.presentingViewController;
        BookSearchTableViewController *bsc = nav.viewControllers[0];
        //NSLog(@"Check of autoload needed section=%lu row=%lu",indexPath.section, indexPath.row);
        [bsc loadMoreBooks];
    }
    
    NSDictionary *book = self.searchResults[indexPath.row];
    
    if (book == nil || [book count] == 0) {
        return;
    }
    
    ReadingListCell *myCell = (ReadingListCell *)cell;
    
    myCell.bookAuthorLabel.text = [book objectForKey:@"author"];
    myCell.bookTitleLabel.text = [book objectForKey:@"title"];
    
    NSString *imageString = [book objectForKey:@"image_url"];
    
    [myCell.bookImageView sd_setImageWithURL:[NSURL URLWithString:imageString] placeholderImage:[UIImage imageNamed:@"light_blue_book_cover"]];
    
    /*NSString *imageString = [book objectForKey:@"image_url"];
    NSLog(@"%@", imageString);
    NSURL *imagePath = [NSURL URLWithString:imageString relativeToURL:nil];
    
    dispatch_queue_t queue = dispatch_queue_create("downloadAsset",NULL);
    
    dispatch_async(queue, ^{
        NSData *data = [NSData dataWithContentsOfURL:imagePath];
        //[NSThread sleepForTimeInterval:3];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIImage *image = [UIImage imageWithData:data];
            myCell.bookImageView.image = image;
        });
    });*/
    
    //UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"add139"]];
    
    UIImageView *imageView = [[UIImageView alloc] init];
    UIImage *image = [UIImage imageNamed:@"Plus-25-Blue"];
    [imageView setImage:image];
    
    // update image view frame width and height.
    imageView.frame = CGRectMake(imageView.frame.origin.x, imageView.frame.origin.y, 25, 25);
    myCell.accessoryView = imageView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 67;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *book = self.searchResults[indexPath.row];
    
    // NEW SAVE
    NSString *title = [book objectForKey:@"title"];
    NSString *description = [book objectForKey:@"description"];
    NSString *author = [book objectForKey:@"author"];
    NSUUID *uuid = [[NSUUID alloc] init];
    NSString *key  = [uuid UUIDString];
    NSString *imageUrlString = [book objectForKey:@"image_url"];
    
    if (title && title.length) {
        // Create Entity
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Book" inManagedObjectContext:self.managedObjectContext];
        
        // Initialize Record
        NSManagedObject *record = [[NSManagedObject alloc] initWithEntity:entity insertIntoManagedObjectContext:self.managedObjectContext];
        
        // Populate Record
        [record setValue:title forKey:@"bookTitle"];
        [record setValue:author forKey:@"bookAuthor"];
        [record setValue:[NSDate date] forKey:@"createdAt"];
        [record setValue:key forKey:@"itemKey"];
        [record setValue:description forKey:@"bookDescription"];
        
        // Save Record
        NSError *error = nil;
        
        // Image Download
        [BookUtils downloadCoverImage:imageUrlString :key];
        
        if ([self.managedObjectContext save:&error]) {
            NSLog(@"Saved Record");

            // Dismiss View Controller
            [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
            
            // Getting the ReadingList Controller via the view hierarchy
            UINavigationController *rootNav = (UINavigationController *)self.presentingViewController.presentingViewController;
            ReadingListController *rl = (ReadingListController *)[rootNav.viewControllers objectAtIndex:0];
            
            // Updateing the reading list table to display the new book
            //Dismiss the search and search results views
            [rl dismissViewControllerAnimated:NO completion:nil];
            
        } else {
            NSLog(@"ERROR: %@", error);
        }
    }
    // EOF NEW SAVE
    return;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
