//
//  main.m
//  ReadingList
//
//  Created by david lobo on 05/11/2015.
//  Copyright (c) 2015 David Lobo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
