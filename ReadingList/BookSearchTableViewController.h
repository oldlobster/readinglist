//
//  BookSearchTableViewController.h
//  Reader
//
//  Created by david lobo on 08/10/2015.
//  Copyright (c) 2015 David Lobo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookSearchTableViewController : UITableViewController <UISearchBarDelegate>

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

- (void)loadMoreBooks;

@end
