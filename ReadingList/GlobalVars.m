//
//  GlobalVars.m
//  FootyApp
//
//  Created by david lobo on 05/08/2015.
//  Copyright (c) 2015 Big Nerd Ranch. All rights reserved.
//

#import "GlobalVars.h"

@implementation GlobalVars

+ (GlobalVars *)sharedInstance {
    static dispatch_once_t onceToken;
    static GlobalVars *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[GlobalVars alloc] init];
    });
    return instance;
}

- (id)init {
    self = [super init];
    if (self) {
        
        // SET ENV HERE
        #if TARGET_IPHONE_SIMULATOR
        self.env = @"local";
        #else
        self.env = @"device";
        #endif
        
        if ([self.env isEqualToString:@"local"]) {
            
        } else {
        
        }
    }
    return self;
}

@end