//
//  BookDetailController.h
//  ReadingList
//
//  Created by david lobo on 05/10/2015.
//  Copyright (c) 2015 David Lobo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface BookDetailController : UIViewController

@property (nonatomic, strong) BOOL(^reloadReadingListTable)();
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSManagedObject *record;

@end
