//
//  ReadingListController.h
//  ReadList
//
//  Created by david lobo on 19/10/2015.
//  Copyright (c) 2015 David Lobo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReadingListController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSIndexPath *selection;

- (IBAction)showSortActionSheet:(id)sender;

@end
