//
//  ReadingListController.m
//  ReadList
//
//  Created by david lobo on 19/10/2015.
//  Copyright (c) 2015 David Lobo. All rights reserved.
//

#import "ReadingListController.h"
#import <CoreData/CoreData.h>
#import "ReadingListCell.h"
#import "BookUtils.h"
#import "BookDetailController.h"
#import "BookSearchTableViewController.h"
#import "EmptyTable.h"
#import "TableUtils.h"

@interface ReadingListController () <NSFetchedResultsControllerDelegate, UIActionSheetDelegate>
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (weak, nonatomic) IBOutlet UISegmentedControl *readingListFilter;
@property (nonatomic) NSArray *sortDescriptors;
@end

@implementation ReadingListController

#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    // Initialize Fetch Request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Book"];
    
    self.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"createdAt" ascending:NO]];
    
    // Add Sort Descriptors
    [fetchRequest setSortDescriptors:self.sortDescriptors];
    
    // Initialize Fetched Results Controller
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    // Configure Fetched Results Controller
    [self.fetchedResultsController setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsController performFetch:&error];
    
    if (error) {
        NSLog(@"Unable to perform fetch.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
    
    [self.readingListFilter addTarget:self
                               action:@selector(reloadReadListTable)
                     forControlEvents:UIControlEventValueChanged];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    //NSLog(@"Prepare for segue");
    if ([segue.identifier isEqualToString:@"addToDoViewController"]) {
        // Obtain Reference to View Controller
        UINavigationController *nc = (UINavigationController *)[segue destinationViewController];
        ReadingListController *vc = (ReadingListController *)[nc topViewController];
        
        // Configure View Controller
        [vc setManagedObjectContext:self.managedObjectContext];
    } else if ([segue.identifier isEqualToString:@"updateToDoViewController"]) {
        // Obtain Reference to View Controller
        //UINavigationController *nc = (UINavigationController *)[segue destinationViewController];
        //BookDetailController *vc = (BookDetailController *)[nc topViewController];
        
        BookDetailController *vc = (BookDetailController *)[segue destinationViewController];
        
        // Configure View Controller
        [vc setManagedObjectContext:self.managedObjectContext];
        
        if (self.selection) {
            // Fetch Record
            NSManagedObject *record = [self.fetchedResultsController objectAtIndexPath:self.selection];
            
            if (record) {
                [vc setRecord:record];
            }
            
            // Reset Selection
            [self setSelection:nil];
        }
    } else if ([segue.identifier isEqualToString:@"bookSearchTable"]) {
        // Obtain Reference to View Controller
        UINavigationController *nc = (UINavigationController *)[segue destinationViewController];
        BookSearchTableViewController *vc = (BookSearchTableViewController *)[nc topViewController];
        
        // Configure View Controller
        [vc setManagedObjectContext:self.managedObjectContext];
    }
}

#pragma mark - UITableViewDatasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger count =  [[self.fetchedResultsController sections] count];
    
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *sections = [self.fetchedResultsController sections];
    id<NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
    
    NSInteger count =  [sectionInfo numberOfObjects];
    //NSLog(@"Section: %lu Rows: %lu", section, count);
    
    if (section == 0) {
        if (count == 0) {
            NSString *imageName = @"Plus-75";
            NSString *message = @"No books to display for this list.  Why not add a new one now?";
            [TableUtils addBackgroundViewFromNib:self.tableView :imageName :message];
            
            return 0;
        }
    }
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.backgroundView = nil;
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"ReadingListCell";
    static NSString *CellNib = @"ReadingListCell";
    
    ReadingListCell *cell = (ReadingListCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellNib owner:self options:nil];
        cell = (ReadingListCell *)[nib objectAtIndex:0];
    }
    
    // Configure Table View Cell
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    // Store Selection
    [self setSelection:indexPath];
    [self performSegueWithIdentifier:@"updateToDoViewController" sender:self];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 67;
}

// Override to support conditional editing of the table view.
// This only needs to be implemented if you are going to be returning NO
// for some items. By default, all items are editable.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObject *record = [self.fetchedResultsController objectAtIndexPath:indexPath];
        NSString *itemKey = (NSString *)[record valueForKey:@"itemKey"];
        
        NSLog(@"deleting itemKey: %@", itemKey);
        if (record) {
            [self.fetchedResultsController.managedObjectContext deleteObject:record];
            [BookUtils deleteImageForItemKey:itemKey];
        }
    }
}

#pragma mark - UIFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    switch (type) {
        case NSFetchedResultsChangeInsert: {
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
        case NSFetchedResultsChangeDelete: {
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
        case NSFetchedResultsChangeUpdate: {
            [self configureCell:(ReadingListCell *)[self.tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
        }
        case NSFetchedResultsChangeMove: {
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
    }
}

#pragma mark - UIAlertViewDelegate

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (actionSheet.tag == 100) {
        if (buttonIndex == 0) {
            self.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"createdAt" ascending:NO]];
        } else if (buttonIndex == 1) {
            self.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"bookTitle" ascending:YES]];
        } else if (buttonIndex == 2) {
            self.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"bookAuthor" ascending:YES]];
        }
        [self reloadReadListTable];
    }
    
    // NSLog(@"Index = %ld - Title = %@", (long)buttonIndex, [actionSheet buttonTitleAtIndex:buttonIndex]);
}

#pragma mark - Custom methods

- (void)configureCell:(ReadingListCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    // Fetch Record
    NSManagedObject *record = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // Update Cell
    //cell.bookTitleLabel.text = @"Test Title";
    [cell.bookTitleLabel setText:[record valueForKey:@"bookTitle"]];
    [cell.bookAuthorLabel setText:[record valueForKey:@"bookAuthor"]];
    
    UIImage *coverImage = [BookUtils coverImageForItemKey:[record valueForKey:@"itemKey"]];
    
    if (coverImage != nil) {
        cell.bookImageView.image = coverImage;
    }
}

- (IBAction)showSortActionSheet:(id)sender {
    NSString *message;
    NSSortDescriptor *sd = (NSSortDescriptor *)self.sortDescriptors[0];
    NSString *key = sd.key;
    
    if ([key isEqualToString:@"createdAt"]) {
        message = @"Recently Added";
    } else if ([key isEqualToString:@"bookTitle"]) {
        message = @"Book Title";
    } else if ([key isEqualToString:@"bookAuthor"]) {
        message = @"Author";
    }
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Sorted By"
                                          message:message
                                          preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *sortDateAction = [UIAlertAction
                                     actionWithTitle:NSLocalizedString(@"Recently Added", @"Recent action")
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction *action)
                                     {
                                         self.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"createdAt" ascending:NO]];
                                         [self reloadReadListTable];
                                     }];
    
    [alertController addAction:sortDateAction];
    
    UIAlertAction *sortTitleAction = [UIAlertAction
                                      actionWithTitle:NSLocalizedString(@"Book Title", @"Title action")
                                      style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction *action)
                                      {
                                          self.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"bookTitle" ascending:YES]];
                                          [self reloadReadListTable];
                                      }];
    
    [alertController addAction:sortTitleAction];
    
    UIAlertAction *sortAuthorAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"Author", @"Author action")
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           self.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"bookAuthor" ascending:YES]];
                                           [self reloadReadListTable];
                                       }];
    
    [alertController addAction:sortAuthorAction];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       //No action
                                   }];
    
    [alertController addAction:cancelAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)reloadReadListTable {
    self.fetchedResultsController = nil;
    
    // Initialize Fetch Request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Book"];
    
    if (self.readingListFilter.selectedSegmentIndex != 0) {
        NSNumber *bookStatusValue;
        
        if (self.readingListFilter.selectedSegmentIndex == 1) {
            bookStatusValue = [NSNumber numberWithInt:0];
        } else if (self.readingListFilter.selectedSegmentIndex == 2) {
            bookStatusValue = [NSNumber numberWithInt:1];
        }
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", @"bookStatus", bookStatusValue];
        [fetchRequest setPredicate:predicate];
    }
    
    // Add Sort Descriptors
    [fetchRequest setSortDescriptors:self.sortDescriptors];
    
    // Initialize Fetched Results Controller
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    // Configure Fetched Results Controller
    [self.fetchedResultsController setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsController performFetch:&error];
    
    if (error) {
        NSLog(@"Unable to perform fetch.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
        [self.tableView reloadData];
    }
}

- (void)addBackgroundView
{
    // Get a reference to the superview
    //UIView *superview = self.tableView.backgroundView;
    UIView *view = [[UIView alloc] init];
    //view.backgroundColor = [UIColor redColor];
    self.tableView.backgroundView = view;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    UILabel *messageLabel = [[UILabel alloc] init];
    //messageLabel.text = @"Your reading list is currently empty.  Please add some books";
    messageLabel.textColor = [UIColor blackColor];
    messageLabel.numberOfLines = 0;
    messageLabel.textAlignment = NSTextAlignmentCenter;
    messageLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:20];
    messageLabel.textColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.38];
    
    NSString *imageName;
    NSString *messageString;
    
    imageName = @"Plus-75";
    messageString = @"No books to display for this list.  Why not add a new one now?";
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:messageString];
    
    messageLabel.attributedText = attributedString;
    
    UIImageView *image = [[UIImageView alloc] init];
    image.image = [UIImage imageNamed:imageName];
    image.frame = CGRectMake(image.frame.origin.x, image.frame.origin.y, 75, 75);
    image.alpha = 0.15;
    
    image.contentMode = UIViewContentModeScaleAspectFit;
    [self.tableView.backgroundView addSubview:image];
    [self.tableView.backgroundView addSubview:messageLabel];
    
    // Create the views and metrics dictionaries
    NSDictionary *metrics = @{@"height":@50.0};
    NSDictionary *views = NSDictionaryOfVariableBindings(messageLabel, image);
    
    messageLabel.translatesAutoresizingMaskIntoConstraints = NO;
    image.translatesAutoresizingMaskIntoConstraints = NO;
    
    // Horizontally 30p from the edges
    [self.tableView.backgroundView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-30.0-[image]-30.0-|" options:NO metrics:metrics views:views]];
    
    // Vertiically 15p from label
    [self.tableView.backgroundView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[image]-15.0-[messageLabel]" options:NO metrics:metrics views:views]];
    
    // Horizontal layout - note the options for aligning the top and bottom of all views
    [self.tableView.backgroundView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-30.0-[messageLabel]-30.0-|" options:NSLayoutFormatAlignAllTop | NSLayoutFormatAlignAllBottom metrics:metrics views:views]];
    
    [self.tableView.backgroundView addConstraint:[NSLayoutConstraint constraintWithItem:messageLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.tableView.backgroundView attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
    [self.tableView.backgroundView addConstraint:[NSLayoutConstraint constraintWithItem:messageLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.tableView.backgroundView attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
}

@end
