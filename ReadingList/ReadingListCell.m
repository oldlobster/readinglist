//
//  ReadingListCell.m
//  ReadingList
//
//  Created by david lobo on 01/10/2015.
//  Copyright (c) 2015 David Lobo. All rights reserved.
//

#import "ReadingListCell.h"


@implementation ReadingListCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
