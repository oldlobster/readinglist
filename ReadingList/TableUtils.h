//
//  TableUtils.h
//  ReadList
//
//  Created by david lobo on 21/10/2015.
//  Copyright (c) 2015 David Lobo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TableUtils : NSObject

+ (void)addBackgroundViewFromNib:(UITableView *)tableView :(NSString *)imageName :(NSString *)message;

@end
