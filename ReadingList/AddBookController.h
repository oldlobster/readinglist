//
//  AddBookController.h
//  ReadingList
//
//  Created by david lobo on 06/10/2015.
//  Copyright (c) 2015 David Lobo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddBookController : UIViewController <UITextFieldDelegate, UITextViewDelegate>

- (IBAction)done:(id)sender;
- (IBAction)cancel:(id)sender;

@end

